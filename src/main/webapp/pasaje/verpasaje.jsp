<%-- 
    Document   : verpasaje
    Created on : 23/06/2022, 4:16:56 p.�m.
    Author     : Daniel
--%>

<%@include file="../utilidades/cabecera.jsp" %>      
<h2 style="text-align:center">Pasajes</h2>
<div class="container">
    <div class="row justify-content-md-center ">
        <div class="col col-lg-2"></div>
        <div class="col-md-auto">
            <a class="btn btn-primary" href="${pageContext.request.contextPath}/pasaje?accion=agregar" role="button">Agregar</a>
        </div>
        <div class="col col-lg-2"></div>
    </div>
    <p></p>
    <table class="table table-primary table-striped table-bordered">
        <tr>
            <th>Id</th>
            <th>Precio</th>
            <th>IdPasajero</th> 
            <th>IdAsiento</th>
            <th>Fecha</th> 
            <th>Acci�n</th>
        </tr>
        <c:forEach var="pasaje" items="${pasajes}">
            <tr>
                <td>${pasaje.id}</td>
                <td>${pasaje.precio}</td>
                <td>${pasaje.idPasajero}</td>
                <td>${pasaje.idAsiento}</td>
                <td>${pasaje.fecha}</td>
                <td>
                    <a class="btn btn-primary" href="${pageContext.request.contextPath}/pasaje?accion=eliminar&id=${pasaje.id}" role="button">Eliminar</a>
                    <a class="btn btn-primary" href="${pageContext.request.contextPath}/pasaje?accion=editar&id=${pasaje.id}" role="button">Actualizar</a>
                </td>
            </tr>
        </c:forEach>
    </table>
</div>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>
<%@include file="../utilidades/pie.jsp" %>