<%@include file="../utilidades/cabecera.jsp" %>
<h2 style="text-align:center">Agregar Pasaje</h2>
<form action="${pageContext.request.contextPath}/pasaje?accion=crear" method="post">
    <div class="formulario">
        <div class="mb-3">
            <label for="exampleInputPassword1" class="form-label">Precio</label>
            <input type="text" class="form-control" id="precio" name="precio">
        </div>

        <div class="input-group mb-3">
            <label class="input-group-text" for="inputGroupSelect01">Id Pasajero</label>
            <select class="form-select" id="idPasajero" name="idPasajero">
                <c:forEach var="pasajero" items="${pasajeros}">
                    <option value="${pasajero.id}">${pasajero.id}</option>
                </c:forEach>
            </select>
        </div>

        <div class="input-group mb-3">
            <label class="input-group-text" for="inputGroupSelect01">Id Asiento</label>
            <select class="form-select" id="idAsiento" name="idAsiento">
                <c:forEach var="asiento" items="${asientos}">
                    <option value="${asiento.id}">${asiento.id}</option>
                </c:forEach>
            </select>
        </div>

        <div class="input-group mb-3">
            <label for="exampleInputPassword1" class="form-label">Fecha</label>
            <div class="input-group date" id="datepicker">
                <input type="text" class="form-control" id="fecha" name="fecha">
                <span class="input-group-append">
                    <span class="input-group-text bg-white">
                        <i class="fa fa-calendar"></i>
                    </span>
                </span>
            </div>
        </div>

        <script type="text/javascript">
            $(function () {
                $('#datepicker').datepicker();
            });
        </script>
        <button type="submit" class="btn btn-primary">Guardar</button>
    </div>
</form>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>
<%@include file="../utilidades/pie.jsp" %>
