<%@include file="../utilidades/cabecera.jsp" %>      
<h2 style="text-align:center">Pasajeros</h2>
<div class="container">
    <div class="row justify-content-md-center ">
        <div class="col col-lg-2">
        </div>
        <div class="col-md-auto">
            <a class="btn btn-primary" href="${pageContext.request.contextPath}/pasajero/agregarpasajero.jsp" role="button">Agregar</a>
        </div>
        <div class="col col-lg-2">
        </div>
    </div>
    <p></p>
    <table class="table table-primary table-striped table-bordered">
        <tr>
            <th>Id</th>
            <th>Nombre</th>
            <th>Apellido</th> 
            <th>Direcci�n</th>
            <th>Correo</th> 
            <th>Acci�n</th>
        </tr>
        <c:forEach var="pasajero" items="${pasajeros}">
            <tr>
                <td>${pasajero.id}</td>
                <td>${pasajero.nombre}</td>
                <td>${pasajero.apellido}</td>
                <td>${pasajero.direccion}</td>
                <td>${pasajero.correo}</td>
                <td>
                    <a class="btn btn-primary" href="${pageContext.request.contextPath}/pasajero?accion=eliminar&id=${pasajero.id}" role="button">Eliminar</a>
                    <a class="btn btn-primary" href="${pageContext.request.contextPath}/pasajero?accion=editar&id=${pasajero.id}" role="button">Actualizar</a>
                </td>
            </tr>
        </c:forEach>
    </table>
</div>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>
<%@include file="../utilidades/pie.jsp" %>