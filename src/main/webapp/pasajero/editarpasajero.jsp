<%-- 
    Document   : editarpasajero
    Created on : 28 jun. 2022, 10:37:10 a. m.
    Author     : Estudiante
--%>

<%@include file="../utilidades/cabecera.jsp" %>
<h2 style="text-align:center">Editar Pasajero</h2>
<form action="${pageContext.request.contextPath}/pasajero?accion=actualizar" method="post">
    <div class="formulario">
        <input type="hidden" value="${pasajero.id}" class="form-control" id="id" name="id">
        <div class="mb-3">
            <label for="exampleInputPassword1" class="form-label">Nombre</label>
            <input type="text" value="${pasajero.nombre}" class="form-control" id="nombre" name="nombre">
        </div>

        <div class="mb-3">
            <label for="exampleInputEmail1" class="form-label">Apellido</label>
            <input type="text" value="${pasajero.apellido}" class="form-control" id="apellido" name="apellido">
        </div>

        <div class="mb-3">
            <label for="exampleInputEmail1" class="form-label">Direccion</label>
            <input type="text" value="${pasajero.direccion}" class="form-control" id="direccion" name="direccion">
        </div>

        <div class="mb-3">
            <label for="exampleInputEmail1" class="form-label">Correo</label>
            <input type="text" value="${pasajero.correo}" class="form-control" id="correo" name="correo">
        </div>

        <div class="mb-3">
            <label for="exampleInputEmail1" class="form-label">Contraseņa</label>
            <input type="password" value="${pasajero.contrasenia}" class="form-control" id="contrasenia" name="contrasenia">
        </div>

        <button type="submit" class="btn btn-primary">Guardar</button>
    </div>
</form>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>
<%@include file="../utilidades/pie.jsp" %>
