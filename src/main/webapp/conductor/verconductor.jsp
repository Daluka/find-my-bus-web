<%-- 
    Document   : verconductor
    Created on : 23/06/2022, 6:54:54 p.�m.
    Author     : Daniel
--%>
<%@include file="../utilidades/cabecera.jsp" %>   
<h2 style="text-align:center">Conductores</h2>
<div class="container">

    <div class="row justify-content-md-center ">
        <div class="col col-lg-2">
        </div>
        <div class="col-md-auto">
            <a class="btn btn-primary" href="${pageContext.request.contextPath}/conductor/agregarconductor.jsp" role="button">Agregar</a>
        </div>
        <div class="col col-lg-2">
        </div>
    </div>
    <p></p>
    <table class="table table-primary table-striped table-bordered">
        <tr>
            <th>Id</th>
            <th>Nombre</th>
            <th>Apellido</th> 
            <th>Direcci�n</th>
            <th>Correo</th> 
            <th>Acci�n</th>
        </tr>
        <c:forEach var="conductor" items="${conductores}">
            <tr>
                <td>${conductor.id}</td>
                <td>${conductor.nombre}</td>
                <td>${conductor.apellido}</td>
                <td>${conductor.direccion}</td>
                <td>${conductor.correo}</td>
                <td>
                    <a class="btn btn-primary" href="${pageContext.request.contextPath}/conductor?accion=eliminar&id=${conductor.id}" role="button">Eliminar</a>
                    <a class="btn btn-primary" href="${pageContext.request.contextPath}/conductor?accion=editar&id=${conductor.id}" role="button">Actualizar</a>
                </td>
            </tr>
        </c:forEach>
    </table>
</div>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>
</body>
</html>