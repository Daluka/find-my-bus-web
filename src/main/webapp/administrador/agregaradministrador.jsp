<%-- 
    Document   : agregaradministrador
    Created on : 28/06/2022, 9:24:46 p.�m.
    Author     : Daniel
--%>
<%@include file="../utilidades/cabecera.jsp" %>
<h2 style="text-align:center">Agregar Administrador</h2>
<form action="${pageContext.request.contextPath}/administrador?accion=crear" method="post">
    <div class="formulario">
        <div class="mb-3">
            <label for="exampleInputPassword1" class="form-label">Id</label>
            <input type="text" class="form-control" id="id" name="id">
        </div>
        <div class="mb-3">
            <label for="exampleInputPassword1" class="form-label">Nombre</label>
            <input type="text" class="form-control" id="nombre" name="nombre">
        </div>

        <div class="mb-3">
            <label for="exampleInputEmail1" class="form-label">Apellido</label>
            <input type="text" class="form-control" id="apellido" name="apellido">
        </div>

        <div class="mb-3">
            <label for="exampleInputEmail1" class="form-label">Direccion</label>
            <input type="text"  class="form-control" id="direccion" name="direccion">
        </div>

        <div class="mb-3">
            <label for="exampleInputEmail1" class="form-label">Correo</label>
            <input type="text"  class="form-control" id="correo" name="correo">
        </div>

        <div class="mb-3">
            <label for="exampleInputEmail1" class="form-label">Contrase�a</label>
            <input type="password" class="form-control" id="contrasenia" name="contrasenia">
        </div>

        <div class="input-group mb-3">
            <label class="input-group-text" for="inputGroupSelect01">Id Compa�ia</label>
            <select class="form-select" id="idCompania" name="idCompania">
                <c:forEach var="compania" items="${companias}">
                    <option value="${compania.id}">${compania.id}</option>
                </c:forEach>
            </select>
        </div>
        <button type="submit" class="btn btn-primary">Guardar</button>
        
    </div>
</form>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>
<%@include file="../utilidades/pie.jsp" %>
