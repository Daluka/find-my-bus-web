<%-- 
    Document   : editarasiento
    Created on : 29/06/2022, 3:15:50 p. m.
    Author     : Daniel
--%>

<%@include file="../utilidades/cabecera.jsp" %>
<h2 style="text-align:center">Editar Asiento</h2>
<form action="${pageContext.request.contextPath}/asiento?accion=actualizar" method="post">
    <div class="formulario">
        <input type="hidden" value="${asiento.id}" class="form-control" id="id" name="id">

        <div class="input-group mb-3">
            <label class="input-group-text" for="inputGroupSelect01">Id Buseta</label>
            <select class="form-select" id="idBuseta" name="idBuseta">
                <option selected="${asiento.idBuseta}">${asiento.idBuseta}</option>
                <c:forEach var="buseta" items="${busetas}">
                    <option value="${buseta.id}">${buseta.id}</option>
                </c:forEach>
            </select>
        </div>
        <div class="mb-3">
            <label for="exampleInputEmail1" class="form-label">Fila</label>
            <input type="text" value="${asiento.fila}" class="form-control" id="fila" name="fila">
        </div>
        <div class="mb-3">
            <label for="exampleInputEmail1" class="form-label">Columna</label>
            <input type="text" value="${asiento.columna}" class="form-control" id="columna" name="columna">
        </div>

        <button type="submit" class="btn btn-primary">Guardar</button>
    </div>
</form>
        
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>
<%@include file="../utilidades/pie.jsp" %>
