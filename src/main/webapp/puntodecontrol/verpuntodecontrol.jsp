<%-- 
    Document   : verpuntodecontrol
    Created on : 23/06/2022, 6:55:26 p. m.
    Author     : Daniel
--%>

<%@include file="../utilidades/cabecera.jsp" %>     
<h2 style="text-align:center">Puntos de control</h2>
<div class="container">
    <div class="row justify-content-md-center ">
        <div class="col col-lg-2">
        </div>
        <div class="col-md-auto">
            <a class="btn btn-primary" href="${pageContext.request.contextPath}/puntodecontrol?accion=agregar" role="button">Agregar</a>
        </div>
        <div class="col col-lg-2">
        </div>
    </div>
    <p></p>
    <table class="table table-primary table-striped table-bordered">
        <tr>
            <th>Id Buseta</th>
            <th>Id Lugar</th>
            <th>Hora</th> 
            <th>Accion</th> 
        </tr>
        <c:forEach var="puntoDeControl" items="${puntosDeControl}">
            <tr>
                <td>${puntoDeControl.idBuseta}</td>
                <td>${puntoDeControl.idLugar}</td>
                <td>${puntoDeControl.hora}</td>
                <td>
                    <a class="btn btn-primary" href="${pageContext.request.contextPath}/puntodecontrol?accion=eliminar&idBuseta=${puntoDeControl.idBuseta}&idLugar=${puntoDeControl.idLugar}" role="button">Eliminar</a>
                    <a class="btn btn-primary" href="${pageContext.request.contextPath}/puntodecontrol?accion=editar&idBuseta=${puntoDeControl.idBuseta}&idLugar=${puntoDeControl.idLugar}" role="button">Actualizar</a>
                </td>
            </tr>
        </c:forEach>
    </table>
</div>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>
<%@include file="../utilidades/pie.jsp" %>