<%-- 
    Document   : editarpuntodecontrol
    Created on : 28/06/2022, 9:07:57 p. m.
    Author     : Daniel
--%>

<%@include file="../utilidades/cabecera.jsp" %>
<h2 style="text-align:center">Editar punto de control</h2>
<form action="${pageContext.request.contextPath}/puntodecontrol?accion=actualizar" method="post">
    <div class="formulario">

        <div class="input-group mb-3">
            <label class="input-group-text" for="inputGroupSelect01">Id Buseta</label>
            <select class="form-select" id="idBuseta" name="idBuseta">
                <option selected="${puntoDeControl.idBuseta}">${puntoDeControl.idBuseta}</option>
                <c:forEach var="buseta" items="${busetas}">
                    <option value="${buseta.id}">${buseta.id}</option>
                </c:forEach>
            </select>
        </div>

        <div class="input-group mb-3">
            <label class="input-group-text" for="inputGroupSelect01">Id Lugar</label>
            <select class="form-select" id="idLugar" name="idLugar">
                <option selected="${puntoDeControl.idLugar}">${puntoDeControl.idLugar}</option>
                <c:forEach var="lugar" items="${lugares}">
                    <option value="${lugar.id}">${lugar.id}</option>
                </c:forEach>
            </select>
        </div>

        <div class="mb-3">
            <label for="exampleInputEmail1" class="form-label">Hora</label>
            <input type="text"  value="${puntoDeControl.hora}"  class="form-control" id="hora" name="hora">
        </div>

        <button type="submit" class="btn btn-primary">Guardar</button>
    </div>
</form>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>
<%@include file="../utilidades/pie.jsp" %>
