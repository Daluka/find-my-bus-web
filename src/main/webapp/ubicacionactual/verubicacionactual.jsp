<%-- 
    Document   : verubicacionactual
    Created on : 23/06/2022, 6:55:38 p. m.
    Author     : Daniel
--%>

<%@include file="../utilidades/cabecera.jsp" %>     
<h2 style="text-align:center">Ubicaciones actuales</h2>
<div class="container">
    <div class="row justify-content-md-center ">
        <div class="col col-lg-2">
        </div>
        <div class="col-md-auto">
            <a class="btn btn-primary" href="${pageContext.request.contextPath}/ubicacionactual/agregarubicacionactual.jsp" role="button">Agregar</a>
        </div>
        <div class="col col-lg-2">
        </div>
    </div>
    <p></p>
    <table class="table table-primary table-striped table-bordered">
        <tr>
            <th>Id</th>
            <th>X value</th>
            <th>Y value</th>
            <th>Z value</th>
            <th>accion</th>
        </tr>
        <c:forEach var="ubicacionActual" items="${ubicacionesActuales}">
            <tr>
                <td>${ubicacionActual.id}</td>
                <td>${ubicacionActual.xValue}</td>
                <td>${ubicacionActual.yValue}</td>
                <td>${ubicacionActual.zValue}</td>
                <td>
                    <a class="btn btn-primary" href="${pageContext.request.contextPath}/ubicacionactual?accion=eliminar&id=${ubicacionActual.id}" role="button">Eliminar</a>
                    <a class="btn btn-primary" href="${pageContext.request.contextPath}/ubicacionactual?accion=editar&id=${ubicacionActual.id}" role="button">Actualizar</a>
                </td>
            </tr>
        </c:forEach>
    </table>
</div>
<%@include file="../utilidades/pie.jsp" %>