<%-- 
    Document   : editarubicacionactual
    Created on : 28/06/2022, 8:38:27 p.�m.
    Author     : Daniel
--%>

<%@include file="../utilidades/cabecera.jsp" %>
<h2 style="text-align:center">Editar ubicaci�n</h2>
<form action="${pageContext.request.contextPath}/ubicacionactual?accion=actualizar" method="post">
    <div class="formulario">
        <input type="hidden" value="${ubicacionActual.id}" class="form-control" id="id" name="id">
        <div class="mb-3">
            <label for="exampleInputPassword1" class="form-label">Valor en X</label>
            <input type="text" value="${ubicacionActual.xValue}" class="form-control" id="xValue" name="xValue">
        </div>

        <div class="mb-3">
            <label for="exampleInputEmail1" class="form-label">Valor en Y</label>
            <input type="text" value="${ubicacionActual.yValue}" class="form-control" id="yValue" name="yValue">
        </div>

        <div class="mb-3">
            <label for="exampleInputEmail1" class="form-label">Valor en Z</label>
            <input type="text" value="${ubicacionActual.zValue}" class="form-control" id="zValue" name="zValue">
        </div>

        <button type="submit" class="btn btn-primary">Guardar</button>
    </div>
</form>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>
<%@include file="../utilidades/pie.jsp" %>
