<%-- 
    Document   : editarlugar
    Created on : 29/06/2022, 03:49:08 PM
    Author     : LUISA LEAL
--%>
<%@include file="../utilidades/cabecera.jsp" %>
<h2 style="text-align:center">Editar Lugar</h2>
<form action="${pageContext.request.contextPath}/lugar?accion=actualizar" method="post">
    <div class="formulario">
        <input type="hidden" value="${lugar.id}" class="form-control" id="id" name="id">
        <div class="mb-3">
            <label for="exampleInputPassword1" class="form-label">Direccion</label>
            <input type="text" value="${lugar.direccion}" class="form-control" id="direccion" name="direccion">
        </div>
        <button type="submit" class="btn btn-primary">Guardar</button>
    </div>
</form>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>
<%@include file="../utilidades/pie.jsp" %>