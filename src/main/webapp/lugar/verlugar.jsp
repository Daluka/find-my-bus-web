<%-- 
    Document   : verlugar
    Created on : 23/06/2022, 6:55:06 p.�m.
    Author     : Daniel
--%>
<%@include file="../utilidades/cabecera.jsp" %>     
<h2 style="text-align:center">Lugares</h2>
<div class="container">
    <div class="row justify-content-md-center ">
        <div class="col col-lg-2">
        </div>
        <div class="col-md-auto">
            <a class="btn btn-primary" href="${pageContext.request.contextPath}/lugar/agregarlugar.jsp" role="button">Agregar</a>
        </div>
        <div class="col col-lg-2">
        </div>
    </div>
    <p></p>
    <table class="table table-primary table-striped table-bordered">
        <tr>
            <th>Id</th>
            <th>Direcci�n</th>
            <th>Acci�n</th>
        </tr>
        <c:forEach var="lugar" items="${lugares}">
            <tr>
                <td>${lugar.id}</td>
                <td>${lugar.direccion}</td>
                <td>
                    <a class="btn btn-primary" href="${pageContext.request.contextPath}/lugar?accion=eliminar&id=${lugar.id}" role="button">Eliminar</a>
                    <a class="btn btn-primary" href="${pageContext.request.contextPath}/lugar?accion=editar&id=${lugar.id}" role="button">Actualizar</a>
                </td>
            </tr>
        </c:forEach>
    </table>
</div>
<%@include file="../utilidades/pie.jsp" %>
