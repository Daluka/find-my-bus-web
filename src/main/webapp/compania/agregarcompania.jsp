<%-- 
    Document   : agregarcompania
    Created on : 29/06/2022, 03:44:49 PM
    Author     : LUISA LEAL
--%>

<%@include file="../utilidades/cabecera.jsp" %>
<h2 style="text-align:center">Agregar Compa�ia</h2>
<form action="${pageContext.request.contextPath}/compania?accion=crear" method="post">
    <div class="formulario">
        <div class="mb-3">
            <label for="exampleInputPassword1" class="form-label">Id</label>
            <input type="text" class="form-control" id="id" name="id">
        </div>
        <div class="mb-3">
            <label for="exampleInputPassword1" class="form-label">Nombre</label>
            <input type="text"  class="form-control" id="nombre" name="nombre">
        </div>

        <div class="mb-3">
            <label for="exampleInputEmail1" class="form-label">Direccion</label>
            <input type="text" class="form-control" id="direccion" name="direccion">
        </div>
        <button type="submit" class="btn btn-primary">Guardar</button>
    </div>
</form>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/utilidades/estilofindmybus.css"/>
<%@include file="../utilidades/pie.jsp" %>
