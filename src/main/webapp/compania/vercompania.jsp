<%-- 
    Document   : vercompania
    Created on : 23/06/2022, 6:54:45 p.�m.
    Author     : Daniel
--%>

<%@include file="../utilidades/cabecera.jsp" %>
<h2 style="text-align:center">Compa�ias</h2>
<div class="container">
    <div class="row justify-content-md-center ">
        <div class="col col-lg-2">
        </div>
        <div class="col-md-auto">
            <a class="btn btn-primary" href="${pageContext.request.contextPath}/compania/agregarcompania.jsp" role="button">Agregar</a>
        </div>
        <div class="col col-lg-2">
        </div>
    </div>
    <p></p>
    <table class="table table-primary table-striped table-bordered">
        <tr>
            <th>Id</th>
            <th>Nombre</th>
            <th>Direcci�n</th>
            <th>Acci�n</th>
        </tr>
        <c:forEach var="compania" items="${companias}">
            <tr>
                <td>${compania.id}</td>
                <td>${compania.nombre}</td>
                <td>${compania.direccion}</td>
                <td>
                    <a class="btn btn-primary" href="${pageContext.request.contextPath}/compania?accion=eliminar&id=${compania.id}" role="button">Eliminar</a>
                    <a class="btn btn-primary" href="${pageContext.request.contextPath}/compania?accion=editar&id=${compania.id}" role="button">Actualizar</a>
                </td>
            </tr>
        </c:forEach>
    </table>
</div>
<%@include file="../utilidades/pie.jsp" %>
