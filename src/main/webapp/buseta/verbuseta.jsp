<%-- 
    Document   : verbuseta
    Created on : 23/06/2022, 6:54:39 p.�m.
    Author     : Daniel
--%>

<%@include file="../utilidades/cabecera.jsp" %>      
<h2 style="text-align:center">Busetas</h2>
<div class="container">
    <div class="row justify-content-md-center ">
        <div class="col col-lg-2">
        </div>
        <div class="col-md-auto">
            <a class="btn btn-primary" href="${pageContext.request.contextPath}/buseta?accion=agregar" role="button">Agregar</a>
        </div>
        <div class="col col-lg-2">
        </div>
    </div>
    <p></p>
    <table class="table table-primary table-striped table-bordered">
        <tr>
            <th>Id</th>
            <th>Placa</th>
            <th>Id Compania</th> 
            <th>Id Conductor</th>
            <th>Id Ubicacion Actual</th>
            <th>Acci�n</th> 
        </tr>
        <c:forEach var="buseta" items="${busetas}">
            <tr>
                <td>${buseta.id}</td>
                <td>${buseta.placa}</td>
                <td>${buseta.idCompania}</td>
                <td>${buseta.idConductor}</td>
                <td>${buseta.idUbicacionActual}</td>
                <td>
                    <a class="btn btn-primary" href="${pageContext.request.contextPath}/buseta?accion=eliminar&id=${buseta.id}" role="button">Eliminar</a>
                    <a class="btn btn-primary" href="${pageContext.request.contextPath}/buseta?accion=editar&id=${buseta.id}" role="button">Actualizar</a>
                </td>
            </tr>
        </c:forEach>
    </table>
</div>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>
<%@include file="../utilidades/pie.jsp" %>