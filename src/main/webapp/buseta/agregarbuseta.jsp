<%-- 
    Document   : agregarbuseta
    Created on : 29/06/2022, 3:18:17 p.�m.
    Author     : Daniel
--%>

<%@include file="../utilidades/cabecera.jsp" %>
<h2 style="text-align:center">Agregar Buseta</h2>
<form action="${pageContext.request.contextPath}/buseta?accion=crear" method="post">
    <div class="formulario">
        <div class="mb-3">
            <label for="exampleInputEmail1" class="form-label">Placa</label>
            <input type="text" class="form-control" id="placa" name="placa">
        </div>

        <div class="input-group mb-3">
            <label class="input-group-text" for="inputGroupSelect01">Id Compa�ia</label>
            <select class="form-select" id="idCompania" name="idCompania">
                <c:forEach var="compania" items="${companias}">
                    <option value="${compania.id}">${compania.nombre}</option>
                </c:forEach>
            </select>
        </div>

        <div class="input-group mb-3">
            <label class="input-group-text" for="inputGroupSelect01">Id Conductor</label>
            <select class="form-select" id="idConductor" name="idConductor">
                <c:forEach var="conductor" items="${conductores}">
                    <option value="${conductor.id}">${conductor.id}</option>
                </c:forEach>
            </select>
        </div>

        <div class="input-group mb-3">
            <label class="input-group-text" for="inputGroupSelect01">Id Ubicacion Actual</label>
            <select class="form-select" id="idUbicacionActual" name="idUbicacionActual">
                <c:forEach var="ubicacionActual" items="${ubicacionesActuales}">
                    <option value="${ubicacionActual.id}">${ubicacionActual.id}</option>
                </c:forEach>
            </select>
        </div>

        <button type="submit" class="btn btn-primary">Guardar</button>
    </div>
</form>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>
<%@include file="../utilidades/pie.jsp" %>