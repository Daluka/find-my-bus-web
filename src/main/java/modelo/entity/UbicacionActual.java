/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package modelo.entity;

/**
 *
 * @author Daniela
 */
public class UbicacionActual {

    private int id;
    private double xValue;
    private double yValue;
    private double zValue;

    public UbicacionActual() {
    }

    public UbicacionActual(int id) {
        this.id = id;
    }
    
    

    public UbicacionActual(int id, double xValue, double yValue, double zValue) {
        this.id = id;
        this.xValue = xValue;
        this.yValue = yValue;
        this.zValue = zValue;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getxValue() {
        return xValue;
    }

    public void setxValue(double xValue) {
        this.xValue = xValue;
    }

    public double getyValue() {
        return yValue;
    }

    public void setyValue(double yValue) {
        this.yValue = yValue;
    }

    public double getzValue() {
        return zValue;
    }

    public void setzValue(double zValue) {
        this.zValue = zValue;
    }

}
