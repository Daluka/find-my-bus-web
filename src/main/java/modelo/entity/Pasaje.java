/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package modelo.entity;

import java.sql.Date;

/**
 *
 * @author Daniela
 */
public class Pasaje {

    private int id;
    private double precio;
    private String idPasajero;
    private int idAsiento;
    private Date fecha;

    public Pasaje() {
    }

    public Pasaje(int id) {
        this.id = id;
    }

    public Pasaje(int id, double precio, String idPasajero, int idAsiento, Date fecha) {
        this.id = id;
        this.precio = precio;
        this.idPasajero = idPasajero;
        this.idAsiento = idAsiento;
        this.fecha = fecha;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public String getIdPasajero() {
        return idPasajero;
    }

    public void setIdPasajero(String idPasajero) {
        this.idPasajero = idPasajero;
    }

    public int getIdAsiento() {
        return idAsiento;
    }

    public void setIdAsiento(int idAsiento) {
        this.idAsiento = idAsiento;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

}
