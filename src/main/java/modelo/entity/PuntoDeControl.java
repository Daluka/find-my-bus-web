/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package modelo.entity;

import java.sql.Time;

/**
 *
 * @author Daniela
 */
public class PuntoDeControl {

    private int idBuseta;
    private int idLugar;
    private Time hora;

    public PuntoDeControl(int idBuseta, int idLugar, Time hora) {
        this.idBuseta = idBuseta;
        this.idLugar = idLugar;
        this.hora = hora;
    }

    public PuntoDeControl(int idBuseta, int idLugar) {
        this.idBuseta = idBuseta;
        this.idLugar = idLugar;
    }

    public int getIdBuseta() {
        return idBuseta;
    }

    public void setIdBuseta(int idBuseta) {
        this.idBuseta = idBuseta;
    }

    public int getIdLugar() {
        return idLugar;
    }

    public void setIdLugar(int idLugar) {
        this.idLugar = idLugar;
    }

    public Time getHora() {
        return hora;
    }

    public void setHora(Time hora) {
        this.hora = hora;
    }

}
