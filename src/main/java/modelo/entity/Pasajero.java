/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package modelo.entity;

/**
 *
 * @author Daniela
 */
public class Pasajero extends Persona {

    public Pasajero() {
    }

    public Pasajero(String id, String nombre, String apellido, String direccion, String correo, String contrasenia) {
        super(id, nombre, apellido, direccion, correo, contrasenia);
    }

    public Pasajero(String id) {
        super(id);
    }

}
