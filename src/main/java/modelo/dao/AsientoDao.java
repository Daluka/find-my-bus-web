/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package modelo.dao;

import modelo.entity.Asiento;
import red.BaseDeDatos;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Daniel
 */
public class AsientoDao implements AsientoServices {

    public static final String SQL_CONSULTA = "SELECT * FROM asiento";
    public static final String SQL_INSERT = "INSERT INTO asiento(id_buseta, fila, columna) VALUES (?,?,?)";
    public static final String SQL_DELETE = "DELETE FROM asiento WHERE id = ?";
    public static final String SQL_UPDATE = "UPDATE asiento SET id_buseta = ?, fila = ?, columna = ? WHERE id = ?";
    public static final String SQL_CONSULTAID = "SELECT * FROM asiento WHERE id = ?";

    @Override
    public int create(Asiento asiento) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet res = null;
        int registros = 0;
        try {
            con = BaseDeDatos.getConnection();
            ps = con.prepareStatement(SQL_INSERT, Statement.RETURN_GENERATED_KEYS);
            ps.setInt(1, asiento.getIdBuseta());
            ps.setInt(2, asiento.getFila());
            ps.setInt(3, asiento.getColumna());
            registros = ps.executeUpdate();
            res = ps.getGeneratedKeys();
            res.next();
            asiento.setId(res.getInt(1));
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                BaseDeDatos.close(ps);
                BaseDeDatos.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return registros;
    }

    @Override
    public List<Asiento> all() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet res = null;
        List<Asiento> asientos = new ArrayList();
        try {
            con = BaseDeDatos.getConnection();
            ps = con.prepareStatement(SQL_CONSULTA);
            res = ps.executeQuery();
            while (res.next()) {
                int id = res.getInt("id");
                int idBuseta = res.getInt("id_buseta");
                int fila = res.getInt("fila");
                int columna = res.getInt("columna");
                Asiento asiento = new Asiento(id, idBuseta, fila, columna);
                asientos.add(asiento);
            }
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                BaseDeDatos.close(res);
                BaseDeDatos.close(ps);
                BaseDeDatos.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return asientos;
    }

    @Override
    public Asiento selectId(Asiento asiento) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet res = null;
        Asiento registroAsiento = null;
        try {
            con = BaseDeDatos.getConnection();
            ps = con.prepareStatement(SQL_CONSULTAID, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.TYPE_FORWARD_ONLY);
            ps.setInt(1, asiento.getId());
            res = ps.executeQuery();
            res.absolute(1);
            int id = res.getInt("id");
            int idBuseta = res.getInt("id_buseta");
            int fila = res.getInt("fila");
            int columna = res.getInt("columna");
            registroAsiento = new Asiento(id, idBuseta, fila, columna);
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                BaseDeDatos.close(res);
                BaseDeDatos.close(ps);
                BaseDeDatos.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return registroAsiento;
    }

    @Override
    public int update(Asiento asiento) {
        Connection con = null;
        PreparedStatement ps = null;
        int registros = 0;
        try {
            con = BaseDeDatos.getConnection();
            ps = con.prepareStatement(SQL_UPDATE);
            ps.setInt(1, asiento.getIdBuseta());
            ps.setInt(2, asiento.getFila());
            ps.setInt(3, asiento.getColumna());
            ps.setInt(4, asiento.getId());
            registros = ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                BaseDeDatos.close(ps);
                BaseDeDatos.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return registros;
    }

    @Override
    public int delete(Asiento asiento) {
        Connection con = null;
        PreparedStatement ps = null;
        int registros = 0;
        try {
            con = BaseDeDatos.getConnection();
            ps = con.prepareStatement(SQL_DELETE);
            ps.setInt(1, asiento.getId());
            registros = ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                BaseDeDatos.close(ps);
                BaseDeDatos.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return registros;
    }

}
