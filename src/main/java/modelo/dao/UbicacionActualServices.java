/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package modelo.dao;

import modelo.entity.UbicacionActual;
import java.util.List;

/**
 *
 * @author Daniel
 */
public interface UbicacionActualServices {

    public int create(UbicacionActual ubicacionActual);

    public List<UbicacionActual> all();

    public UbicacionActual selectId(UbicacionActual ubicacionActual);

    public int update(UbicacionActual ubicacionActual);

    public int delete(UbicacionActual ubicacionActual);
}
