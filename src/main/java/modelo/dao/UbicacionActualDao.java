/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package modelo.dao;

import modelo.entity.UbicacionActual;
import red.BaseDeDatos;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Daniel
 */
public class UbicacionActualDao implements UbicacionActualServices {

    public static final String SQL_INSERT = "INSERT INTO ubicacion_actual(x_value, y_value, z_value) VALUES (?,?,?)";
    public static final String SQL_CONSULTA = "SELECT * FROM ubicacion_actual";
    public static final String SQL_CONSULTAID = "SELECT * FROM ubicacion_actual WHERE id = ?";
    public static final String SQL_DELETE = "DELETE FROM ubicacion_actual WHERE id = ?";
    public static final String SQL_UPDATE = "UPDATE ubicacion_actual SET x_value = ?, y_value = ?, z_value = ? WHERE id = ?";

    @Override
    public int create(UbicacionActual ubicacionActual) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet res = null;
        int registros = 0;
        try {
            con = BaseDeDatos.getConnection();
            ps = con.prepareStatement(SQL_INSERT, Statement.RETURN_GENERATED_KEYS);           
            ps.setDouble(1, ubicacionActual.getxValue());
            ps.setDouble(2, ubicacionActual.getyValue());
            ps.setDouble(3, ubicacionActual.getzValue());
            registros = ps.executeUpdate();
            res = ps.getGeneratedKeys();
            res.next();
            ubicacionActual.setId(res.getInt(1));
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                BaseDeDatos.close(ps);
                BaseDeDatos.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return registros;
    }

    @Override
    public List<UbicacionActual> all() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet res = null;
        List<UbicacionActual> ubicacionesActuales = new ArrayList();
        try {
            con = BaseDeDatos.getConnection();
            ps = con.prepareStatement(SQL_CONSULTA);
            res = ps.executeQuery();
            while (res.next()) {
                int id = res.getInt("id");
                double xValue = res.getDouble("x_value");
                double yValue = res.getDouble("y_value");
                double zValue = res.getDouble("z_value");
                UbicacionActual ubicacionActual = new UbicacionActual(id, xValue, yValue, zValue);
                ubicacionesActuales.add(ubicacionActual);
            }
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                BaseDeDatos.close(res);
                BaseDeDatos.close(ps);
                BaseDeDatos.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return ubicacionesActuales;

    }

    @Override
    public UbicacionActual selectId(UbicacionActual ubicacionActual) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet res = null;
        UbicacionActual registroUbicacion = null;
        try {
            con = BaseDeDatos.getConnection();
            ps = con.prepareStatement(SQL_CONSULTAID, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.TYPE_FORWARD_ONLY);
            ps.setInt(1, ubicacionActual.getId());
            res = ps.executeQuery();
            res.absolute(1);
            int id = res.getInt("id");
            double xValue = res.getDouble("x_value");
            double yValue = res.getDouble("y_value");
            double zValue = res.getDouble("z_value");
            registroUbicacion = new UbicacionActual(id, xValue, yValue, zValue);
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                BaseDeDatos.close(res);
                BaseDeDatos.close(ps);
                BaseDeDatos.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return registroUbicacion;
    }

    @Override
    public int update(UbicacionActual ubicacionActual) {
        Connection con = null;
        PreparedStatement ps = null;
        int registros = 0;
        try {
            con = BaseDeDatos.getConnection();
            ps = con.prepareStatement(SQL_UPDATE);
            ps.setDouble(1, ubicacionActual.getxValue());
            ps.setDouble(2, ubicacionActual.getyValue());
            ps.setDouble(3, ubicacionActual.getzValue());
            ps.setDouble(4, ubicacionActual.getId());
            registros = ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                BaseDeDatos.close(ps);
                BaseDeDatos.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return registros;
    }

    @Override
    public int delete(UbicacionActual ubicacionActual) {
        Connection con = null;
        PreparedStatement ps = null;
        int registros = 0;
        try {
            con = BaseDeDatos.getConnection();
            ps = con.prepareStatement(SQL_DELETE);
            ps.setInt(1, ubicacionActual.getId());
            registros = ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                BaseDeDatos.close(ps);
                BaseDeDatos.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return registros;
    }

}
