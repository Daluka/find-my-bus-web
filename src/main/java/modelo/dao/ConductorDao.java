/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package modelo.dao;

import modelo.entity.Conductor;
import red.BaseDeDatos;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Daniel
 */
public class ConductorDao implements ConductorServices {

    public static final String SQL_CONSULTA = "SELECT * FROM conductor C, persona P WHERE C.id = P.id";
    public static final String SQL_INSERT = "INSERT INTO conductor(id) VALUES (?)";
    public static final String SQL_PINSERT = "INSERT INTO persona(id, nombre, apellido, direccion, correo, contrasenia) VALUES (?,?,?,?,?,?)";
    public static final String SQL_DELETE = "DELETE FROM conductor WHERE id = ?";
    public static final String SQL_UPDATE = "UPDATE conductor SET nombre = ?, apellido = ?, direccion = ?, correo = ?, contrasenia = ? WHERE id = ?";
    public static final String SQL_PUPDATE = "UPDATE persona SET nombre = ?, apellido = ?, direccion = ?, correo = ?, contrasenia = ? WHERE id = ?";
    public static final String SQL_CONSULTAID = "SELECT * FROM conductor C, persona P WHERE C.id = ? AND C.id = P.id";

    @Override
    public int create(Conductor conductor) {
        Connection con = null;
        PreparedStatement ps = null;
        int registros = 0;
        try {
            con = BaseDeDatos.getConnection();
            ps = con.prepareStatement(SQL_PINSERT);
            ps.setString(1, conductor.getId());
            ps.setString(2, conductor.getNombre());
            ps.setString(3, conductor.getApellido());
            ps.setString(4, conductor.getDireccion());
            ps.setString(5, conductor.getCorreo());
            ps.setString(6, conductor.getContrasenia());
            registros = ps.executeUpdate();
            ps = con.prepareStatement(SQL_INSERT);
            ps.setString(1, conductor.getId());
            registros = ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                BaseDeDatos.close(ps);
                BaseDeDatos.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return registros;
    }

    @Override
    public List<Conductor> all() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet res = null;
        List<Conductor> conductores = new ArrayList();
        try {
            con = BaseDeDatos.getConnection();
            ps = con.prepareStatement(SQL_CONSULTA);
            res = ps.executeQuery();
            while (res.next()) {
                String id = res.getString("id");
                String nombre = res.getString("nombre");
                String apellido = res.getString("apellido");
                String correo = res.getString("correo");
                String direccion = res.getString("direccion");
                String contrasenia = res.getString("contrasenia");
                Conductor conductor = new Conductor(id, nombre, apellido, direccion, correo, contrasenia);
                conductores.add(conductor);
            }
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                BaseDeDatos.close(res);
                BaseDeDatos.close(ps);
                BaseDeDatos.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return conductores;
    }

    @Override
    public Conductor selectId(Conductor conductor) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet res = null;
        Conductor registroConductor = null;
        try {
            con = BaseDeDatos.getConnection();
            ps = con.prepareStatement(SQL_CONSULTAID, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.TYPE_FORWARD_ONLY);
            ps.setString(1, conductor.getId());
            res = ps.executeQuery();
            res.absolute(1);
            String id = res.getString("id");
            String nombre = res.getString("nombre");
            String apellido = res.getString("apellido");
            String correo = res.getString("correo");
            String direccion = res.getString("direccion");
            String contrasenia = res.getString("contrasenia");
            registroConductor = new Conductor(id, nombre, apellido, direccion, correo, contrasenia);
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                BaseDeDatos.close(res);
                BaseDeDatos.close(ps);
                BaseDeDatos.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return registroConductor;
    }

    @Override
    public int update(Conductor conductor) {
        Connection con = null;
        PreparedStatement ps = null;
        int registros = 0;
        try {
            con = BaseDeDatos.getConnection();
            ps = con.prepareStatement(SQL_PUPDATE);
            ps.setString(1, conductor.getNombre());
            ps.setString(2, conductor.getApellido());
            ps.setString(3, conductor.getDireccion());
            ps.setString(4, conductor.getCorreo());
            ps.setString(5, conductor.getContrasenia());
            ps.setString(6, conductor.getId());
            registros = ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                BaseDeDatos.close(ps);
                BaseDeDatos.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return registros;
    }

    @Override
    public int delete(Conductor conductor) {
        Connection con = null;
        PreparedStatement ps = null;
        int registros = 0;
        try {
            con = BaseDeDatos.getConnection();
            ps = con.prepareStatement(SQL_DELETE);
            ps.setString(1, conductor.getId());
            registros = ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                BaseDeDatos.close(ps);
                BaseDeDatos.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return registros;
    }
}
