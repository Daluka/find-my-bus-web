/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package modelo.dao;

import java.util.List;
import modelo.entity.Administrador;

/**
 *
 * @author 
 */
public interface AdministradorServices {
    
    public int create(Administrador administrador);

    public List<Administrador> all();

    public Administrador selectId(Administrador administrador);

    public int update(Administrador administrador);

    public int delete(Administrador administrador);
}
