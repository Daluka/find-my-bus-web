/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package modelo.dao;

import java.util.List;
import modelo.entity.Pasajero;

/**
 *
 * @author Daniel
 */
public interface PasajeroServices {

    public int create(Pasajero pasajero);

    public List<Pasajero> all();

    public Pasajero selectId(Pasajero pasajero);

    public int update(Pasajero pasajero);

    public int delete(Pasajero pasajero);
}
