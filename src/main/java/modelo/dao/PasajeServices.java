/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package modelo.dao;

import modelo.entity.Pasaje;
import java.util.List;

/**
 *
 * @author Daniel
 */
public interface PasajeServices {

    public int create(Pasaje pasaje);

    public List<Pasaje> all();

    public Pasaje selectId(Pasaje pasaje);

    public int update(Pasaje pasaje);

    public int delete(Pasaje pasaje);
}
