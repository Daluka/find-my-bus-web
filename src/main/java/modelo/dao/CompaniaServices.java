/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package modelo.dao;

import modelo.entity.Compania;
import java.util.List;

/**
 *
 * @author Daniela
 */
public interface CompaniaServices {
    
    public int create (Compania compania);
    public List <Compania> all();
    public Compania selectId(Compania compania);
    public int update(Compania compania);
    public int delete(Compania compania);
   
    
}
