/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package modelo.dao;

import modelo.entity.Conductor;
import java.util.List;

/**
 *
 * @author Daniel
 */
public interface ConductorServices {
   public int create(Conductor conductor);

    public List<Conductor> all();

    public Conductor selectId(Conductor conductor);

    public int update(Conductor conductor);

    public int delete(Conductor conductor);  
}
