/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package modelo.dao;

import java.util.List;
import modelo.entity.Buseta;

/**
 *
 * @author Daniel
 */
public interface BusetaServices {
    
    public int create(Buseta buseta);

    public List<Buseta> all();

    public Buseta selectId(Buseta buseta);

    public int update(Buseta buseta);

    public int delete(Buseta buseta);
}
