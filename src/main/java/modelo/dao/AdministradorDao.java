/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package modelo.dao;

import red.BaseDeDatos;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import modelo.entity.Administrador;

/**
 *
 * @author Daniel
 */
public class AdministradorDao implements AdministradorServices {

    public static final String SQL_CONSULTA = "SELECT * FROM administrador A, persona P WHERE A.id = P.id";
    public static final String SQL_INSERT = "INSERT INTO administrador(id, id_compania) VALUES (?,?)";
    public static final String SQL_PINSERT = "INSERT INTO persona(id, nombre, apellido, direccion, correo, contrasenia) VALUES (?,?,?,?,?,?)";
    public static final String SQL_DELETE = "DELETE FROM administrador WHERE id = ?";
    public static final String SQL_UPDATE = "UPDATE administrador SET id_compania = ? WHERE id = ?";
    public static final String SQL_PUPDATE = "UPDATE persona SET nombre = ?, apellido = ?, direccion = ?, correo = ?, contrasenia = ? WHERE id = ?";
    public static final String SQL_CONSULTAID = "SELECT * FROM administrador A, persona P WHERE A.id = ? AND A.id = P.id ";

    @Override
    public int create(Administrador administrador) {
        Connection con = null;
        PreparedStatement ps = null;
        int registros = 0;
        try {
            con = BaseDeDatos.getConnection();
            ps = con.prepareStatement(SQL_PINSERT);
            ps.setString(1, administrador.getId());
            ps.setString(2, administrador.getNombre());
            ps.setString(3, administrador.getApellido());
            ps.setString(4, administrador.getDireccion());
            ps.setString(5, administrador.getCorreo());
            ps.setString(6, administrador.getContrasenia());
            registros = ps.executeUpdate();
            ps = con.prepareStatement(SQL_INSERT);
            ps.setString(1, administrador.getId());
            ps.setString(2, administrador.getIdCompania());
            registros = ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                BaseDeDatos.close(ps);
                BaseDeDatos.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return registros;
    }

    @Override
    public List<Administrador> all() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet res = null;
        List<Administrador> administradores = new ArrayList();
        try {
            con = BaseDeDatos.getConnection();
            ps = con.prepareStatement(SQL_CONSULTA);
            res = ps.executeQuery();
            while (res.next()) {
                String id = res.getString("id");
                String nombre = res.getString("nombre");
                String apellido = res.getString("apellido");
                String correo = res.getString("correo");
                String direccion = res.getString("direccion");
                String contrasenia = res.getString("contrasenia");
                String idCompania = res.getString("id_compania");
                Administrador administrador = new Administrador(id, nombre, apellido, direccion, correo, contrasenia, idCompania);
                administradores.add(administrador);
            }
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                BaseDeDatos.close(res);
                BaseDeDatos.close(ps);
                BaseDeDatos.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return administradores;
    }

    @Override
    public Administrador selectId(Administrador administrador) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet res = null;
        Administrador registroAdministrador = null;
        try {
            con = BaseDeDatos.getConnection();
            ps = con.prepareStatement(SQL_CONSULTAID, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.TYPE_FORWARD_ONLY);
            ps.setString(1, administrador.getId());
            res = ps.executeQuery();
            res.absolute(1);
            String id = res.getString("id");
            String nombre = res.getString("nombre");
            String apellido = res.getString("apellido");
            String correo = res.getString("correo");
            String direccion = res.getString("direccion");
            String contrasenia = res.getString("contrasenia");
            String idCompania = res.getString("id_compania");
            registroAdministrador = new Administrador(id, nombre, apellido, direccion, correo, contrasenia, idCompania);
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                BaseDeDatos.close(res);
                BaseDeDatos.close(ps);
                BaseDeDatos.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return registroAdministrador;
    }

    @Override
    public int update(Administrador administrador) {
        Connection con = null;
        PreparedStatement ps = null;
        int registros = 0;
        try {
            con = BaseDeDatos.getConnection();
            ps = con.prepareStatement(SQL_PUPDATE);
            ps.setString(1, administrador.getNombre());
            ps.setString(2, administrador.getApellido());
            ps.setString(3, administrador.getDireccion());
            ps.setString(4, administrador.getCorreo());
            ps.setString(5, administrador.getContrasenia());
            ps.setString(6, administrador.getId());
            registros = ps.executeUpdate();
            ps = con.prepareStatement(SQL_UPDATE);
            ps.setString(1, administrador.getIdCompania());
            ps.setString(2, administrador.getId());
            registros = ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                BaseDeDatos.close(ps);
                BaseDeDatos.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return registros;
    }

    @Override
    public int delete(Administrador administrador) {
        Connection con = null;
        PreparedStatement ps = null;
        int registros = 0;
        try {
            con = BaseDeDatos.getConnection();
            ps = con.prepareStatement(SQL_DELETE);
            ps.setString(1, administrador.getId());
            registros = ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                BaseDeDatos.close(ps);
                BaseDeDatos.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return registros;
    }

}
