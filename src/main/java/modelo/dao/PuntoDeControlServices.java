/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package modelo.dao;

import modelo.entity.PuntoDeControl;
import java.util.List;

/**
 *
 * @author Daniel
 */
public interface PuntoDeControlServices {

    public int create(PuntoDeControl puntoDeControl);

    public List<PuntoDeControl> all();

    public PuntoDeControl selectId(PuntoDeControl puntoDeControl);

    public int update(PuntoDeControl pntoDeControl);

    public int delete(PuntoDeControl puntoDeControl);
}
