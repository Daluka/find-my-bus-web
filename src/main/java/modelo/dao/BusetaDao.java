/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package modelo.dao;

import modelo.entity.Buseta;
import red.BaseDeDatos;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Daniel
 */
public class BusetaDao implements BusetaServices {

    public static final String SQL_CONSULTA = "SELECT * FROM buseta";
    public static final String SQL_INSERT = "INSERT INTO buseta(placa, id_compania, id_conductor, id_ubicacion_actual) VALUES (?,?,?,?)";
    public static final String SQL_DELETE = "DELETE FROM buseta WHERE id = ?";
    public static final String SQL_UPDATE = "UPDATE buseta SET placa = ?, id_compania = ?, id_conductor = ?, id_ubicacion_actual = ? WHERE id = ?";
    public static final String SQL_CONSULTAID = "SELECT * FROM buseta WHERE id = ?";

    @Override
    public int create(Buseta buseta) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet res = null;
        int registros = 0;
        try {
            con = BaseDeDatos.getConnection();
            ps = con.prepareStatement(SQL_INSERT, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, buseta.getPlaca());
            ps.setString(2, buseta.getIdCompania());
            ps.setString(3, buseta.getIdConductor());
            ps.setInt(4, buseta.getIdUbicacionActual());
            registros = ps.executeUpdate();
            res = ps.getGeneratedKeys();
            res.next();
            buseta.setId(res.getInt(1));
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                BaseDeDatos.close(ps);
                BaseDeDatos.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return registros;
    }

    @Override
    public List<Buseta> all() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet res = null;
        List<Buseta> busetas = new ArrayList();
        try {
            con = BaseDeDatos.getConnection();
            ps = con.prepareStatement(SQL_CONSULTA);
            res = ps.executeQuery();
            while (res.next()) {
                int id = res.getInt("id");
                String placa = res.getString("placa");
                String idCompania = res.getString("id_compania");
                String idConductor = res.getString("id_conductor");
                int idUbicacionActual = res.getInt("id_ubicacion_actual");
                Buseta buseta = new Buseta(id, placa, idCompania, idConductor, idUbicacionActual);
                busetas.add(buseta);
            }
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                BaseDeDatos.close(res);
                BaseDeDatos.close(ps);
                BaseDeDatos.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return busetas;
    }

    @Override
    public Buseta selectId(Buseta buseta) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet res = null;
        Buseta registroAsiento = null;
        try {
            con = BaseDeDatos.getConnection();
            ps = con.prepareStatement(SQL_CONSULTAID, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.TYPE_FORWARD_ONLY);
            ps.setInt(1, buseta.getId());
            res = ps.executeQuery();
            res.absolute(1);
            int id = res.getInt("id");
            String placa = res.getString("placa");
            String idCompania = res.getString("id_compania");
            String idConductor = res.getString("id_conductor");
            int idUbicacionActual = res.getInt("id_ubicacion_actual");
            registroAsiento = new Buseta(id, placa, idCompania, idConductor, idUbicacionActual);
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                BaseDeDatos.close(res);
                BaseDeDatos.close(ps);
                BaseDeDatos.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return registroAsiento;
    }

    @Override
    public int update(Buseta buseta) {
        Connection con = null;
        PreparedStatement ps = null;
        int registros = 0;
        try {
            con = BaseDeDatos.getConnection();
            ps = con.prepareStatement(SQL_UPDATE);
            ps.setString(1, buseta.getPlaca());
            ps.setString(2, buseta.getIdCompania());
            ps.setString(3, buseta.getIdConductor());
            ps.setInt(4, buseta.getIdUbicacionActual());
            ps.setInt(5, buseta.getId());
            registros = ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                BaseDeDatos.close(ps);
                BaseDeDatos.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return registros;
    }

    @Override
    public int delete(Buseta buseta) {
        Connection con = null;
        PreparedStatement ps = null;
        int registros = 0;
        try {
            con = BaseDeDatos.getConnection();
            ps = con.prepareStatement(SQL_DELETE);
            ps.setInt(1, buseta.getId());
            registros = ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                BaseDeDatos.close(ps);
                BaseDeDatos.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return registros;
    }

}
