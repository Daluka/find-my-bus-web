/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package modelo.dao;

import java.util.List;
import modelo.entity.Asiento;

/**
 *
 * @author Daniel
 */
public interface AsientoServices {
    
    public int create(Asiento asiento);

    public List<Asiento> all();

    public Asiento selectId(Asiento asiento);

    public int update(Asiento asiento);

    public int delete(Asiento asiento);
}
