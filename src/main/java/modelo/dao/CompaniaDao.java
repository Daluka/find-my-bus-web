/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package modelo.dao;

import modelo.entity.Compania;
import red.BaseDeDatos;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Daniela
 */
public class CompaniaDao implements CompaniaServices {

    public static final String SQL_CONSULTA = "SELECT * FROM compania";
    public static final String SQL_INSERT = "INSERT INTO compania(id, nombre, direccion) VALUES (?,?,?)";
    public static final String SQL_DELETE = "DELETE FROM compania WHERE id = ?";
    public static final String SQL_UPDATE = "UPDATE compania SET nombre = ?,direccion = ? WHERE id = ?";
    public static final String SQL_CONSULTAID = "SELECT * FROM compania WHERE id = ?";

    @Override
    public int create(Compania compania) {
        Connection con = null;
        PreparedStatement ps = null;
        int registros = 0;
        try {
            con = BaseDeDatos.getConnection();
            ps = con.prepareStatement(SQL_INSERT);
            ps.setString(1, compania.getId());
            ps.setString(2, compania.getNombre());
            ps.setString(3, compania.getDireccion());
            registros = ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                BaseDeDatos.close(ps);
                BaseDeDatos.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return registros;
    }

    @Override
    public List<Compania> all() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet res = null;
        List<Compania> companias = new ArrayList();
        try {
            con = BaseDeDatos.getConnection();
            ps = con.prepareStatement(SQL_CONSULTA);
            res = ps.executeQuery();
            while (res.next()) {
                String id = res.getString("id");
                String nombre = res.getString("nombre");
                String direccion = res.getString("direccion");
                Compania compania = new Compania(id, nombre, direccion);
                companias.add(compania);
            }
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                BaseDeDatos.close(res);
                BaseDeDatos.close(ps);
                BaseDeDatos.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return companias;
    }

    @Override
    public Compania selectId(Compania compania) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet res = null;
        Compania registroCompania = null;
        try {
            con = BaseDeDatos.getConnection();
            ps = con.prepareStatement(SQL_CONSULTAID, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.TYPE_FORWARD_ONLY);
            ps.setString(1, compania.getId());
            res = ps.executeQuery();
            res.absolute(1);
            String id = res.getString("id");
            String nombre = res.getString("nombre");
            String direccion = res.getString("direccion");
            registroCompania = new Compania(id, nombre, direccion);
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                BaseDeDatos.close(res);
                BaseDeDatos.close(ps);
                BaseDeDatos.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return registroCompania;
    }

    @Override
    public int update(Compania compania) {
        Connection con = null;
        PreparedStatement ps = null;
        int registros = 0;
        try {
            con = BaseDeDatos.getConnection();
            ps = con.prepareStatement(SQL_UPDATE);
            ps.setString(1, compania.getNombre());
            ps.setString(2, compania.getDireccion());
            ps.setString(3, compania.getId());
            registros = ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                BaseDeDatos.close(ps);
                BaseDeDatos.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return registros;
    }

    @Override
    public int delete(Compania compania) {
        Connection con = null;
        PreparedStatement ps = null;
        int registros = 0;
        try {
            con = BaseDeDatos.getConnection();
            ps = con.prepareStatement(SQL_DELETE);
            ps.setString(1, compania.getId());
            registros = ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
        } finally {
            try {
                BaseDeDatos.close(ps);
                BaseDeDatos.close(con);
            } catch (SQLException ex) {
                ex.printStackTrace(System.out);
            }
        }
        return registros;
    }

}
