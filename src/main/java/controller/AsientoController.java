/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controller;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import modelo.dao.AsientoDao;
import modelo.dao.BusetaDao;
import modelo.entity.Asiento;
import modelo.entity.Buseta;

/**
 *
 * @author Daniel
 */
@WebServlet("/asiento")
public class AsientoController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String accion = req.getParameter("accion");
        if (accion != null) {
            switch (accion) {
                case "eliminar":
                    eliminar(req, resp);
                    break;
                case "consultar":
                    consultar(req, resp);
                    break;
                case "editar":
                    buscarEditar(req, resp);
                    break;
                case "agregar":
                    buscarAgregar(req, resp);
                    break;
            }
        } else {
            consultar(req, resp);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String accion = req.getParameter("accion");
        switch (accion) {
            case "crear":
                crear(req, resp);
                break;
            case "actualizar":
                actualizar(req, resp);
                break;
        }
    }

    private void eliminar(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int id = Integer.parseInt(req.getParameter("id"));
        int registro = new AsientoDao().delete(new Asiento(id));
        consultar(req, resp);
    }

    private void consultar(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        AsientoDao asientoDao = new AsientoDao();
        List<Asiento> asientos = asientoDao.all();
        req.setAttribute("asientos", asientos);
        req.getRequestDispatcher("asiento/verasiento.jsp").forward(req, resp);

    }

    private void crear(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int idBuseta = Integer.parseInt(req.getParameter("idBuseta"));
        int fila = Integer.parseInt(req.getParameter("fila"));
        int colunmna = Integer.parseInt(req.getParameter("columna"));
        Asiento asiento = new Asiento(0, idBuseta, fila, colunmna);
        new AsientoDao().create(asiento);
        consultar(req, resp);
    }

    private void actualizar(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int id = Integer.parseInt(req.getParameter("id"));
        int idBuseta = Integer.parseInt(req.getParameter("idBuseta"));
        int fila = Integer.parseInt(req.getParameter("fila"));
        int colunmna = Integer.parseInt(req.getParameter("columna"));
        Asiento asiento = new Asiento(id, idBuseta, fila, colunmna);
        new AsientoDao().update(asiento);
        consultar(req, resp);
    }

    private void buscarEditar(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int id = Integer.parseInt(req.getParameter("id"));
        Asiento asiento = new AsientoDao().selectId(new Asiento(id));
        req.setAttribute("asiento", asiento);
        BusetaDao busetaDao = new BusetaDao();
        List<Buseta> busetas = busetaDao.all();
        req.setAttribute("busetas", busetas);
        req.getRequestDispatcher("asiento/editarasiento.jsp").forward(req, resp);
    }

    private void buscarAgregar(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        BusetaDao busetaDao = new BusetaDao();
        List<Buseta> busetas = busetaDao.all();
        req.setAttribute("busetas", busetas);
        req.getRequestDispatcher("asiento/agregarasiento.jsp").forward(req, resp);
    }

}
