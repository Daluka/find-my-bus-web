/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controller;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Time;
import java.time.LocalTime;
import java.util.List;
import modelo.dao.BusetaDao;
import modelo.dao.LugarDao;
import modelo.dao.PuntoDeControlDao;
import modelo.entity.Buseta;
import modelo.entity.Lugar;
import modelo.entity.PuntoDeControl;

/**
 *
 * @author Daniel
 */
@WebServlet("/puntodecontrol")
public class PuntoDeControlController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String accion = req.getParameter("accion");
        if (accion != null) {
            switch (accion) {
                case "eliminar":
                    eliminar(req, resp);
                    break;
                case "consultar":
                    consultar(req, resp);
                    break;
                case "editar":
                    buscarEditar(req, resp);
                    break;
                case "agregar":
                    buscarAgregar(req, resp);
                    break;
            }
        } else {
            consultar(req, resp);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String accion = req.getParameter("accion");
        switch (accion) {
            case "crear":
                crear(req, resp);
                break;
            case "actualizar":
                actualizar(req, resp);
                break;
        }
    }

    private void eliminar(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int idBuseta = Integer.parseInt(req.getParameter("idBuseta"));
        int idLugar = Integer.parseInt(req.getParameter("idLugar"));
        int registro = new PuntoDeControlDao().delete(new PuntoDeControl(idBuseta, idLugar));
        consultar(req, resp);
    }

    private void consultar(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        PuntoDeControlDao puntoDeControlDao = new PuntoDeControlDao();
        List<PuntoDeControl> puntosDeControl = puntoDeControlDao.all();
        req.setAttribute("puntosDeControl", puntosDeControl);
        req.getRequestDispatcher("puntodecontrol/verpuntodecontrol.jsp").forward(req, resp);

    }

    private void crear(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int idBuseta = Integer.parseInt(req.getParameter("idBuseta"));
        int idLugar = Integer.parseInt(req.getParameter("idLugar"));
        String horaStr = req.getParameter("hora");
        LocalTime hora = LocalTime.parse(horaStr);
        PuntoDeControl puntoDeControl = new PuntoDeControl(idBuseta, idLugar, Time.valueOf(hora));
        new PuntoDeControlDao().create(puntoDeControl);
        consultar(req, resp);
    }

    private void actualizar(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int idBuseta = Integer.parseInt(req.getParameter("idBuseta"));
        int idLugar = Integer.parseInt(req.getParameter("idLugar"));
        String horaStr = req.getParameter("hora");
        LocalTime hora = LocalTime.parse(horaStr);
        PuntoDeControl puntoDeControl = new PuntoDeControl(idBuseta, idLugar, Time.valueOf(hora));
        new PuntoDeControlDao().update(puntoDeControl);
        consultar(req, resp);
    }

    private void buscarEditar(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int idBuseta = Integer.parseInt(req.getParameter("idBuseta"));
        int idLugar = Integer.parseInt(req.getParameter("idLugar"));
        PuntoDeControl puntoDeControl = new PuntoDeControlDao().selectId(new PuntoDeControl(idBuseta, idLugar));
        req.setAttribute("puntoDeControl", puntoDeControl);
        BusetaDao busetaDao = new BusetaDao();
        List<Buseta> busetas = busetaDao.all();
        req.setAttribute("busetas", busetas);
        LugarDao lugarDao = new LugarDao();
        List<Lugar> lugares = lugarDao.all();
        req.setAttribute("lugares", lugares);
        req.getRequestDispatcher("puntodecontrol/editarpuntodecontrol.jsp").forward(req, resp);
    }

    private void buscarAgregar(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        BusetaDao busetaDao = new BusetaDao();
        List<Buseta> busetas = busetaDao.all();
        req.setAttribute("busetas", busetas);
        LugarDao lugarDao = new LugarDao();
        List<Lugar> lugares = lugarDao.all();
        req.setAttribute("lugares", lugares);
        req.getRequestDispatcher("puntodecontrol/agregarpuntodecontrol.jsp").forward(req, resp);
    }

}
