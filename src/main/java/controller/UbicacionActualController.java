/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controller;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import modelo.dao.UbicacionActualDao;
import modelo.entity.UbicacionActual;

/**
 *
 * @author Daniel
 */
@WebServlet("/ubicacionactual")
public class UbicacionActualController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String accion = req.getParameter("accion");
        if (accion != null) {
            switch (accion) {
                case "eliminar":
                    eliminar(req, resp);
                    break;
                case "consultar":
                    consultar(req, resp);
                    break;
                case "editar":
                    buscarEditar(req, resp);
                    break;
            }
        } else {
            consultar(req, resp);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String accion = req.getParameter("accion");
        switch (accion) {
            case "crear":
                crear(req, resp);
                break;
            case "actualizar":
                actualizar(req, resp);
                break;
        }
    }

    private void eliminar(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int id = Integer.parseInt(req.getParameter("id"));
        int registro = new UbicacionActualDao().delete(new UbicacionActual(id));
        consultar(req, resp);
    }

    private void consultar(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        UbicacionActualDao ubicacionActualDao = new UbicacionActualDao();
        List<UbicacionActual> ubicacionesActuales = ubicacionActualDao.all();
        req.setAttribute("ubicacionesActuales", ubicacionesActuales);
        req.getRequestDispatcher("ubicacionactual/verubicacionactual.jsp").forward(req, resp);

    }

    private void crear(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        double xValue = Double.parseDouble(req.getParameter("xValue"));
        double yValue = Double.parseDouble(req.getParameter("yValue"));
        double zValue = Double.parseDouble(req.getParameter("zValue"));
        UbicacionActual ubicacionActual = new UbicacionActual(0, xValue, yValue, zValue);
        new UbicacionActualDao().create(ubicacionActual);
        consultar(req, resp);
    }

    private void actualizar(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int id = Integer.parseInt(req.getParameter("id"));
        double xValue = Double.parseDouble(req.getParameter("xValue"));
        double yValue = Double.parseDouble(req.getParameter("yValue"));
        double zValue = Double.parseDouble(req.getParameter("zValue"));
        UbicacionActual ubicacionActual = new UbicacionActual(id, xValue, yValue, zValue);
        new UbicacionActualDao().update(ubicacionActual);
        consultar(req, resp);
    }

    private void buscarEditar(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int id = Integer.parseInt(req.getParameter("id"));
        UbicacionActual ubicacionActual = new UbicacionActualDao().selectId(new UbicacionActual(id));
        req.setAttribute("ubicacionActual", ubicacionActual);
        req.getRequestDispatcher("ubicacionactual/editarubicacionactual.jsp").forward(req, resp);
    }
}
