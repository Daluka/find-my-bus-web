/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controller;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import modelo.dao.AdministradorDao;
import modelo.dao.CompaniaDao;
import modelo.entity.Administrador;
import modelo.entity.Compania;

/**
 *
 * @author Daniel
 */
@WebServlet("/administrador")
public class AdministradorController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String accion = req.getParameter("accion");
        if (accion != null) {
            switch (accion) {
                case "eliminar":
                    eliminar(req, resp);
                    break;
                case "consultar":
                    consultar(req, resp);
                    break;
                case "editar":
                    buscarEditar(req, resp);
                    break;
                case "agregar":
                    buscarAgregar(req, resp);
                    break;
            }
        } else {
            consultar(req, resp);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String accion = req.getParameter("accion");
        switch (accion) {
            case "crear":
                crear(req, resp);
                break;
            case "actualizar":
                actualizar(req, resp);
                break;
        }
    }

    private void eliminar(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String id = req.getParameter("id");
        int registro = new AdministradorDao().delete(new Administrador(id));
        consultar(req, resp);
    }

    private void consultar(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        AdministradorDao administradorDao = new AdministradorDao();
        List<Administrador> administradores = administradorDao.all();
        req.setAttribute("administradores", administradores);
        req.getRequestDispatcher("administrador/veradministrador.jsp").forward(req, resp);

    }

    private void crear(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String id = req.getParameter("id");
        String nombre = req.getParameter("nombre");
        String apellido = req.getParameter("apellido");
        String direccion = req.getParameter("direccion");
        String correo = req.getParameter("correo");
        String contrasenia = req.getParameter("contrasenia");
        String idCompania = req.getParameter("idCompania");
        Administrador administrador = new Administrador(id, nombre, apellido, direccion, correo, contrasenia, idCompania);
        new AdministradorDao().create(administrador);
        consultar(req, resp);
    }

    private void actualizar(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String id = req.getParameter("id");
        String nombre = req.getParameter("nombre");
        String apellido = req.getParameter("apellido");
        String direccion = req.getParameter("direccion");
        String correo = req.getParameter("correo");
        String contrasenia = req.getParameter("contrasenia");
        String idCompania = req.getParameter("idCompania");
        Administrador administrador = new Administrador(id, nombre, apellido, direccion, correo, contrasenia, idCompania);
        new AdministradorDao().update(administrador);
        consultar(req, resp);
    }

    private void buscarEditar(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String id = req.getParameter("id");
        Administrador administrador = new AdministradorDao().selectId(new Administrador(id));
        req.setAttribute("administrador", administrador);
        CompaniaDao companiaDao = new CompaniaDao();
        List<Compania> companias = companiaDao.all();
        req.setAttribute("companias", companias);
        req.getRequestDispatcher("administrador/editaradministrador.jsp").forward(req, resp);
    }

    private void buscarAgregar(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        CompaniaDao companiaDao = new CompaniaDao();
        List<Compania> companias = companiaDao.all();
        req.setAttribute("companias", companias);
        req.getRequestDispatcher("administrador/agregaradministrador.jsp").forward(req, resp);
    }

}
