/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controller;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import modelo.dao.PasajeroDao;
import modelo.entity.Pasajero;

/**
 *
 * @author Daniel
 */
@WebServlet("/pasajero")
public class PasajeroController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String accion = req.getParameter("accion");
        if (accion != null) {
            switch (accion) {
                case "eliminar":
                    eliminar(req, resp);
                    break;
                case "consultar":
                    consultar(req, resp);
                    break;
                case "editar":
                    buscarEditar(req, resp);
                    break;
            }
        } else {
            consultar(req, resp);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String accion = req.getParameter("accion");
        switch (accion) {
            case "crear":
                crear(req, resp);
                break;
            case "actualizar":
                actualizar(req, resp);
                break;
        }
    }

    private void eliminar(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String id = req.getParameter("id");
        int registro = new PasajeroDao().delete(new Pasajero(id));
        consultar(req, resp);
    }

    private void consultar(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        PasajeroDao pasajeroDao = new PasajeroDao();
        List<Pasajero> pasajeros = pasajeroDao.all();
        req.setAttribute("pasajeros", pasajeros);
        req.getRequestDispatcher("pasajero/verpasajero.jsp").forward(req, resp);

    }

    private void crear(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String id = req.getParameter("id");
        String nombre = req.getParameter("nombre");
        String apellido = req.getParameter("apellido");
        String direccion = req.getParameter("direccion");
        String correo = req.getParameter("correo");
        String contrasenia = req.getParameter("contrasenia");
        Pasajero pasajero = new Pasajero(id, nombre, apellido, direccion, correo, contrasenia);
        new PasajeroDao().create(pasajero);
        consultar(req, resp);
    }

    private void actualizar(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String id = req.getParameter("id");
        String nombre = req.getParameter("nombre");
        String apellido = req.getParameter("apellido");
        String direccion = req.getParameter("direccion");
        String correo = req.getParameter("correo");
        String contrasenia = req.getParameter("contrasenia");
        Pasajero pasajero = new Pasajero(id, nombre, apellido, direccion, correo, contrasenia);
        new PasajeroDao().update(pasajero);
        consultar(req, resp);
    }

    private void buscarEditar(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String id = req.getParameter("id");
        Pasajero pasajero = new PasajeroDao().selectId(new Pasajero(id));
        req.setAttribute("pasajero", pasajero);
        req.getRequestDispatcher("pasajero/editarpasajero.jsp").forward(req, resp);
    }

}
