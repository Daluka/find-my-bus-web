/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controller;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import modelo.dao.LugarDao;
import modelo.entity.Lugar;

/**
 *
 * @author Daniel
 */
@WebServlet("/lugar")
public class LugarController extends HttpServlet{
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String accion = req.getParameter("accion");
        if (accion != null) {
            switch (accion) {
                case "eliminar":
                    eliminar(req, resp);
                    break;
                case "consultar":
                    consultar(req, resp);
                    break;
                case "editar":
                    buscarEditar(req, resp);
                    break;
            }
        } else {
            consultar(req, resp);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String accion = req.getParameter("accion");
        switch (accion) {
            case "crear":
                crear(req, resp);
                break;
            case "actualizar":
                actualizar(req, resp);
                break;
        }
    }

    private void eliminar(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int id = Integer.parseInt(req.getParameter("id"));
        int registro = new LugarDao().delete(new Lugar(id));
        consultar(req, resp);
    }

    private void consultar(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        LugarDao lugarDao = new LugarDao();
        List<Lugar> lugares = lugarDao.all();
        req.setAttribute("lugares", lugares);
        req.getRequestDispatcher("lugar/verlugar.jsp").forward(req, resp);

    }

    private void crear(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String direccion = req.getParameter("direccion");
        Lugar lugar = new Lugar(0, direccion);
        new LugarDao().create(lugar);
        consultar(req, resp);
    }

    private void actualizar(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int id = Integer.parseInt(req.getParameter("id"));
        String direccion = req.getParameter("direccion");
        Lugar lugar = new Lugar(id, direccion);
        new LugarDao().update(lugar);
        consultar(req, resp);
    }

    private void buscarEditar(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int id = Integer.parseInt(req.getParameter("id"));
        Lugar lugar = new LugarDao().selectId(new Lugar(id));
        req.setAttribute("lugar", lugar);
        req.getRequestDispatcher("lugar/editarlugar.jsp").forward(req, resp);
    }
}
