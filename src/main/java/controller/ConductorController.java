/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controller;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import modelo.dao.ConductorDao;
import modelo.entity.Conductor;

/**
 *
 * @author Daniel
 */
@WebServlet("/conductor")
public class ConductorController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String accion = req.getParameter("accion");
        if (accion != null) {
            switch (accion) {
                case "eliminar":
                    eliminar(req, resp);
                    break;
                case "consultar":
                    consultar(req, resp);
                    break;
                case "editar":
                    buscarEditar(req, resp);
                    break;
            }
        } else {
            consultar(req, resp);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String accion = req.getParameter("accion");
        switch (accion) {
            case "crear":
                crear(req, resp);
                break;
            case "actualizar":
                actualizar(req, resp);
                break;
        }
    }

    private void eliminar(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String id = req.getParameter("id");
        int registro = new ConductorDao().delete(new Conductor(id));
        consultar(req, resp);
    }

    private void consultar(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ConductorDao conductorDao = new ConductorDao();
        List<Conductor> conductores = conductorDao.all();
        req.setAttribute("conductores", conductores);
        req.getRequestDispatcher("conductor/verconductor.jsp").forward(req, resp);

    }

    private void crear(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String id = req.getParameter("id");
        String nombre = req.getParameter("nombre");
        String apellido = req.getParameter("apellido");
        String direccion = req.getParameter("direccion");
        String correo = req.getParameter("correo");
        String contrasenia = req.getParameter("contrasenia");
        Conductor conductor = new Conductor(id, nombre, apellido, direccion, correo, contrasenia);
        new ConductorDao().create(conductor);
        consultar(req, resp);
    }

    private void actualizar(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String id = req.getParameter("id");
        String nombre = req.getParameter("nombre");
        String apellido = req.getParameter("apellido");
        String direccion = req.getParameter("direccion");
        String correo = req.getParameter("correo");
        String contrasenia = req.getParameter("contrasenia");
        Conductor conductor = new Conductor(id, nombre, apellido, direccion, correo, contrasenia);
        new ConductorDao().update(conductor);
        consultar(req, resp);
    }

    private void buscarEditar(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String id = req.getParameter("id");
        Conductor conductor = new ConductorDao().selectId(new Conductor(id));
        req.setAttribute("conductor", conductor);
        req.getRequestDispatcher("conductor/editarconductor.jsp").forward(req, resp);
    }
}
