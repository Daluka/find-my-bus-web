/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controller;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import modelo.dao.AsientoDao;
import modelo.dao.PasajeDao;
import modelo.dao.PasajeroDao;
import modelo.entity.Asiento;
import modelo.entity.Pasaje;
import modelo.entity.Pasajero;

/**
 *
 * @author Daniel
 */
@WebServlet("/pasaje")
public class PasajeController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String accion = req.getParameter("accion");
        if (accion != null) {
            switch (accion) {
                case "eliminar":
                    eliminar(req, resp);
                    break;
                case "consultar":
                    consultar(req, resp);
                    break;
                case "editar":
                    buscarEditar(req, resp);
                    break;
                case "agregar":
                    buscarAgregar(req, resp);
                    break;
            }
        } else {
            consultar(req, resp);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String accion = req.getParameter("accion");
        switch (accion) {
            case "crear":
                crear(req, resp);
                break;
            case "actualizar":
                actualizar(req, resp);
                break;
        }
    }

    private void eliminar(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int id = Integer.parseInt(req.getParameter("id"));
        int registro = new PasajeDao().delete(new Pasaje(id));
        consultar(req, resp);
    }

    private void consultar(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        PasajeDao pasajeDao = new PasajeDao();
        List<Pasaje> pasajes = pasajeDao.all();
        req.setAttribute("pasajes", pasajes);
        req.getRequestDispatcher("pasaje/verpasaje.jsp").forward(req, resp);

    }

    private void crear(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            int id = 0;
            double precio = Double.parseDouble(req.getParameter("precio"));
            String idPasajero = req.getParameter("idPasajero");
            int idAsiento = Integer.parseInt(req.getParameter("idAsiento"));
            String fechaStr = req.getParameter("fecha");
            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
            Date fecha = new Date(sdf.parse(fechaStr).getTime());
            Pasaje pasaje = new Pasaje(id, precio, idPasajero, idAsiento, fecha);
            new PasajeDao().create(pasaje);
            consultar(req, resp);
        } catch (ParseException ex) {
            Logger.getLogger(PasajeController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void actualizar(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            int id = Integer.parseInt(req.getParameter("id"));
            double precio = Double.parseDouble(req.getParameter("precio"));
            String idPasajero = req.getParameter("idPasajero");
            int idAsiento = Integer.parseInt(req.getParameter("idAsiento"));
            String fechaStr = req.getParameter("fecha");
            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
            Date fecha = new Date(sdf.parse(fechaStr).getTime());
            Pasaje pasaje = new Pasaje(id, precio, idPasajero, idAsiento, fecha);
            new PasajeDao().update(pasaje);
            consultar(req, resp);
        } catch (ParseException ex) {
            Logger.getLogger(PasajeController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void buscarEditar(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int id = Integer.parseInt(req.getParameter("id"));
        Pasaje pasaje = new PasajeDao().selectId(new Pasaje(id));
        req.setAttribute("pasaje", pasaje);
        AsientoDao asientoDao = new AsientoDao();
        List<Asiento> asientos = asientoDao.all();
        req.setAttribute("asientos", asientos);
        PasajeroDao pasajeroDao = new PasajeroDao();
        List<Pasajero> pasajeros = pasajeroDao.all();
        req.setAttribute("pasajeros", pasajeros);
        req.getRequestDispatcher("pasaje/editarpasaje.jsp").forward(req, resp);
    }

    private void buscarAgregar(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        AsientoDao asientoDao = new AsientoDao();
        List<Asiento> asientos = asientoDao.all();
        req.setAttribute("asientos", asientos);
        PasajeroDao pasajeroDao = new PasajeroDao();
        List<Pasajero> pasajeros = pasajeroDao.all();
        req.setAttribute("pasajeros", pasajeros);
        req.getRequestDispatcher("pasaje/agregarpasaje.jsp").forward(req, resp);
    }

}
