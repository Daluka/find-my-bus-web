/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controller;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import modelo.dao.BusetaDao;
import modelo.dao.CompaniaDao;
import modelo.dao.ConductorDao;
import modelo.dao.UbicacionActualDao;
import modelo.entity.Buseta;
import modelo.entity.Compania;
import modelo.entity.Conductor;
import modelo.entity.UbicacionActual;

/**
 *
 * @author Daniel
 */
@WebServlet("/buseta")
public class BusetaController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String accion = req.getParameter("accion");
        if (accion != null) {
            switch (accion) {
                case "eliminar":
                    eliminar(req, resp);
                    break;
                case "consultar":
                    consultar(req, resp);
                    break;
                case "editar":
                    buscarEditar(req, resp);
                    break;
                case "agregar":
                    buscarAgregar(req, resp);
                    break;
            }
        } else {
            consultar(req, resp);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String accion = req.getParameter("accion");
        switch (accion) {
            case "crear":
                crear(req, resp);
                break;
            case "actualizar":
                actualizar(req, resp);
                break;
        }
    }

    private void eliminar(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int id = Integer.parseInt(req.getParameter("id"));
        int registro = new BusetaDao().delete(new Buseta(id));
        consultar(req, resp);
    }

    private void consultar(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        BusetaDao busetaDao = new BusetaDao();
        List<Buseta> busetas = busetaDao.all();
        req.setAttribute("busetas", busetas);
        req.getRequestDispatcher("buseta/verbuseta.jsp").forward(req, resp);

    }

    private void crear(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String placa = req.getParameter("placa");
        String idCompania = req.getParameter("idCompania");
        String idConductor = req.getParameter("idConductor");
        int idUbicacionActual = Integer.parseInt(req.getParameter("idUbicacionActual"));
        Buseta buseta = new Buseta(0, placa, idCompania, idConductor, idUbicacionActual);
        new BusetaDao().create(buseta);
        consultar(req, resp);
    }

    private void actualizar(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int id = Integer.parseInt(req.getParameter("id"));
        String placa = req.getParameter("placa");
        String idCompania = req.getParameter("idCompania");
        String idConductor = req.getParameter("idConductor");
        int idUbicacionActual = Integer.parseInt(req.getParameter("idUbicacionActual"));
        Buseta buseta = new Buseta(id, placa, idCompania, idConductor, idUbicacionActual);
        new BusetaDao().update(buseta);
        consultar(req, resp);
    }

    private void buscarEditar(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int id = Integer.parseInt(req.getParameter("id"));
        Buseta buseta = new BusetaDao().selectId(new Buseta(id));
        req.setAttribute("buseta", buseta);

        CompaniaDao companiaDao = new CompaniaDao();
        List<Compania> companias = companiaDao.all();
        req.setAttribute("companias", companias);

        ConductorDao conductorDao = new ConductorDao();
        List<Conductor> conductores = conductorDao.all();
        req.setAttribute("conductores", conductores);

        UbicacionActualDao ubicacionActualDao = new UbicacionActualDao();
        List<UbicacionActual> ubicacionesActuales = ubicacionActualDao.all();
        req.setAttribute("ubicacionesActuales", ubicacionesActuales);

        req.getRequestDispatcher("buseta/editarbuseta.jsp").forward(req, resp);
    }

    private void buscarAgregar(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        CompaniaDao companiaDao = new CompaniaDao();
        List<Compania> companias = companiaDao.all();
        req.setAttribute("companias", companias);

        ConductorDao conductorDao = new ConductorDao();
        List<Conductor> conductores = conductorDao.all();
        req.setAttribute("conductores", conductores);

        UbicacionActualDao ubicacionActualDao = new UbicacionActualDao();
        List<UbicacionActual> ubicacionesActuales = ubicacionActualDao.all();
        req.setAttribute("ubicacionesActuales", ubicacionesActuales);

        req.getRequestDispatcher("buseta/agregarbuseta.jsp").forward(req, resp);
    }

}
